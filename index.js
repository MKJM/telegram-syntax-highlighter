require('dotenv').config()
const domainPing = require("domain-ping");
const { Telegraf } = require('telegraf')
const request = require('request');
var fs = require('fs');

const bot = new Telegraf(process.env.BOT_TOKEN)

async function postCode(code, ctx) {
  // Default options are marked with 
  let url ='https://carbonara.now.sh/api/cook/' 
  let pngData 
  let config = {
      "paddingVertical":"10px",
      "paddingHorizontal":"17px",
      "backgroundImage":null,
      "backgroundImageSelection":null,
      "backgroundMode":"color",
      "backgroundColor":"rgba(21,23,24,1)",
      "dropShadow":false,
      "dropShadowOffsetY":"20px",
      "dropShadowBlurRadius":"68px",
      "theme":"seti",
      "windowTheme":"none",
      "language":"auto",
      "fontFamily":"Hack",
      "fontSize":"14px",
      "lineHeight":"133%",
      "windowControls":false,
      "widthAdjustment":true,
      "lineNumbers":false,
      "firstLineNumber":1,
      "exportSize":"2x",
      "watermark":false,
      "squaredImage":false,
      "hiddenCharacters":false,
      "name":"",
      "width":680
  }
  let body = JSON.stringify({
      ...config,
      "code": code
  })
  let filename = `${ctx.from.id}_${ctx.update.update_id}.png`
    request(
    { method: 'POST', uri: url, headers: { 'content-type': 'application/json' }, body: body })
    .on('response', function(response) {
	console.log(`Fetching PNG for user ${ctx.from.id}. Server responded with status code: `, response.statusCode)
    })
    .on('error', function(err) {
	console.log("An error has occured:", err)
    })
    .pipe(fs.createWriteStream(filename)).on('finish', function() { 
	console.log(`Sending PNG to user ${ctx.from.id}`)
	ctx.replyWithPhoto({source: `./${filename}`})
	fs.unlinkSync(filename)
	console.log(`PNG successfully sent and deleted from the file system`)
    })
}

bot.help((ctx) => ctx.reply('Usage: /code {code}\nProject\'s website: https://gitlab.com/MKJM/telegram-syntax-highlighter\n\nHuge thanks to:\nhttps://carbon.now.sh\nhttps://github.com/petersolopov/carbonara'))

// Consider this a Polish easter egg
bot.hears('życia nasze nic nie warte', (ctx) => ctx.reply('Eviva l\'arte'))

bot.start((ctx) => {
    ctx.replyWithPhoto({source: "./plug.png"})
    ctx.reply("Hi!")
    ctx.reply("To create a code snippet like this one, simply type in /code followed by your code snippet.")
})

bot.launch()

bot.command('code', (ctx) => {
    console.log(`User ${ctx.from.id} issued /code`)
    let code = ctx.update.message.text.split(' ').slice(1).join(' ');
    return postCode(code, ctx)
})
/*
bot.command('ping', (ctx) => {
    //console.log(ctx.update.message.text);
    let link = ctx.update.message.text.split(' ')[1];
    domainPing(link)
	.then((res) => {
	    postCode(JSON.stringify(res, null, 2), ctx)
	})
	.catch((error) => {
	    console.error(error);
	});
    return null
})
*/


// Enable graceful stop
process.once('SIGINT', () => bot.stop('SIGINT'))
process.once('SIGTERM', () => bot.stop('SIGTERM'))
