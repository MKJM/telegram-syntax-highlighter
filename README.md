# Telegram Syntax Highlighter Bot
![Example of syntax highlighting](plug.png)


### Usage
Simply invoke the `/code` command, followed by a code snippet you wish to have highlighted.

### Examples

```
/code function fibonacci(num)
{
  var num1=0;
  var num2=1;
  var sum;
  var i=0;
  for (i = 0; i < num; i++)
    {
      sum=num1+num2;
      num1=num2;
      num2=sum;
    }
    return num2;
}
                    
console.log(fibonacci(155));
```

![A simple Fibonacci algorithm](fib.png)

### Credit
Huge thanks to 
- https://carbon.now.sh
- https://github.com/petersolopov/carbonara
- https://telegraf.js.org/
